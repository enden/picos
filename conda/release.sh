#!/bin/bash -l

#-------------------------------------------------------------------------------
# Copyright (C) 2018 Maximilian Stahlberg
#
# This file is part of PICOS Release Scripts.
#
# PICOS Release Scripts are free software: you can redistribute it and/or modify
# them under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PICOS Release Scripts are distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# This script builds Conda packages for all relevant platforms and uploads them
# to Anaconda Cloud. To use the script, you need a working Conda distribution
# and an Anaconda user account that has access to the ${grpname} account.
#-------------------------------------------------------------------------------

set -e
cd "$(dirname "${BASH_SOURCE[0]}")"


# Base name of the package that will be built.
pkgname="picos"

# Platforms to upload packages for.
platforms="linux-32 linux-64 win-32 win-64 osx-64"

# Name of the Anaconda user or group account to publish at.
grpname="picos"

# Output directory for built packages, relative to the recipe directory.
builddir="pkg"

# Name of the temporary conda environment.
buildenv="${pkgname}-build"


echo ">>> Removing an existing build environment, if present."
conda remove -y -n "${buildenv}" --all

echo ">>> Creating the temporary build environment."
conda create -y -n "${buildenv}" git conda-build anaconda-client

echo ">>> Activating the build environment."
conda activate "${buildenv}"

echo ">>> Building the packages."
condacmd="conda build . --no-anaconda-upload --output-folder ${builddir}"
${condacmd}
startpkgfiles="$(${condacmd} --output)"

echo ">>> Converting the packages for other OS."
for pkgfile in ${startpkgfiles}; do
	conda convert ${pkgfile} -p all -o "${builddir}"
done

echo ">>> Uploading the packages to Anaconda Cloud."
pkgfiles=""
for pkgfile in ${startpkgfiles}; do
	pkgfilename="$(basename "${pkgfile}")"
	for platform in ${platforms}; do
		pkgfiles+=" ${builddir}/${platform}/${pkgfilename}"
	done
done
if anaconda upload --force -u "${grpname}" ${pkgfiles}; then
	echo "(-) Upload successful."
	exitcode=0
else
	echo "(E) Upload failed."
	exitcode=1
fi

echo ">>> Dectivating the build environment."
conda deactivate

echo ">>> Removing the build environment."
conda remove -y -n "${buildenv}" --all

exit ${exitcode}
